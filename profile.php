<?php
  session_start();

  if (!$_SESSION['user']) {
    header('Location: /');
  }

?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="style.css">
  <title>Document</title>
</head>
<body>
<div class="page page-profile">
  <div class="profile">
<!--    <img class="profile-logo" src="--><?//= $_SESSION['user']['avatar'] ?><!--" width="100" alt="profile_img">-->
    <h2 class="profile-fullname">Welcome, <?= $_SESSION['user']['login'] ?></h2>
    <a class="profile-email" href="#" ><?= $_SESSION['user']['email'] ?></a>
    <a href="vendor/logout.php" class="logout">Exit</a>
  </div>
  <!--<div class="sidebar">
    <ul class="menu">
      <li class="menu__item">Test</li>
      <li class="menu__item">Test</li>
      <li class="menu__item">Test</li>
      <li class="menu__item">Test</li>
      <li class="menu__item">Test</li>
      <li class="menu__item">Test</li>
    </ul>
  </div>-->
</div>
</body>
</html>

