<?php
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="style.css">
  <title>Document</title>
</head>
<body>
  <nav class="navbar">
    <a href="/"><img class="logo" src="https://dcassetcdn.com/design_img/1559024/551167/551167_7840631_1559024_911ff84c_image.png" alt=""></a>
    <div class="auth">
      <a class="signup link" href="register.php">Signup</a>
      <a class="signin link" href="login.php">Signin</a>
    </div>
  </nav>
</body>
</html>
