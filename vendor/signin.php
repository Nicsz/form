<?php
  session_start();

  require_once("connect.php");

  if (isset($_POST['submit'])) {
    $connect = Config::connect();

    $login = $_POST['login'];
    $password = md5($_POST['password']);

    if(verifyDetails($connect, $login, $password)) {
//      $_SESSION['login'] = $login;

      header("Location: ../profile.php");
    } else {
//      echo "Your pass or login are incorrect";
      $_SESSION['message'] = '
        <div class="alert alert-danger" role="alert">Wrong login or password</div>
      ';
      header('Location: ../login.php');
    }
  }

  function verifyDetails($connect, $login, $password)
  {
    $query = $connect->prepare("
      SELECT * FROM `users` WHERE login=:login AND password=:password
    ");

    $query->bindParam(":login", $login);
    $query->bindParam(":password", $password);

    $query->execute();

    if ($query->rowCount() > 0) {

      $user = $query->fetch(PDO::FETCH_ASSOC);

      /*foreach ($user as $item) {
        var_dump($item['id']);
        die();
      }*/
      /*echo "<pre>";
      var_dump($user);
      die();*/

      $_SESSION['user']  = [
        "id"        => $user['id'],
//        "full_name" => $user['full_name'],
        "login"     => $user['login'],
        "email"     => $user['email'],
//        "avatar"    => $user['avatar'],
      ];

      /*echo "<pre>";
      var_dump($_SESSION['user']);
      die();*/

      return true;
    } else {
      return false;
    }
  }