<?php
  session_start();

  require_once("connect.php");

  if (isset($_POST['register'])) {
    $connect = Config::connect();

    $login = $_POST['login'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $password_confirm = $_POST['password_confirm'];

    if ($password === $password_confirm) {

      $password = md5($password);

      if(insertDetails($connect, $login, $email, $password)) {
//        echo "details Inserted Successfully";
        $_SESSION['message'] = 'Registration completed successfully';
        header('Location: /');
      }
    } else {
//      echo "Your pass is incorrect";
      $_SESSION['message'] = 'Passwords mismatch';
      header('Location: ../register.php');
    }
  }

  function insertDetails($connect, $login, $email, $password)
  {
    $query = $connect->prepare("
      INSERT INTO users (login, email, password) 
      VALUES(:login, :email, :password)
    ");

    $query->bindParam(":login", $login);
    $query->bindParam(":email", $email);
    $query->bindParam(":password", $password);

    return $query->execute();
  }

