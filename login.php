<?php
  session_start();

  if ($_SESSION['user']) {
    header('Location: profile.php');
  }
?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="style.css">
  <title>Login</title>
</head>
<body>
  <nav class="navbar">
    <a href="/"><img class="logo" src="https://dcassetcdn.com/design_img/1559024/551167/551167_7840631_1559024_911ff84c_image.png" alt=""></a>
    <div class="auth">
      <a class="signup link" href="register.php">Signup</a>
      <a class="signin link" href="login.php">Signin</a>
    </div>
  </nav>
  <div class="content">
    <form class="form-login form" action="vendor/signin.php" method="post">
      <input type="text" name="login" placeholder="Enter Your login">
      <input type="password" name="password" placeholder="Enter Your password">
      <button type="submit" name="submit">Submit</button>

      <?php
      if (isset($_SESSION['message'])) {
        echo '
              <p class="message">' . $_SESSION['message'] . '</p>
            ';
      }
      unset($_SESSION['message']);
      ?>
    </form>
  </div>
</body>
</html>
